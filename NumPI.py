from numpy import linspace,sin,pi,int16
import numpy 
from scipy.io.wavfile import write
import pygame
import time
# from pylab import plot,show,axis

# tone synthesis
def note(freq, len, amp=1, rate=44100):
	t = linspace(0,len,len*rate)
	data = sin(2*pi*freq*t)*amp
	
	return data.astype(int16) # two byte integers
# def chord (note1,note2)

# return 
def noteNumtoName(num, leng, amp=1, rate=44100):
	# Note map and freq of different notes
	# http://python.dzone.com/articles/sound-synthesis-numpy
	# 0	A	440
	# 	B flat	466
	# 1	B	494
	# 2	C	523
	# 3	C sharp	554
	# 4	D	587
	# 5	D sharp	622
	# 6	E	659
	# 7	F	698
	# 8	F sharp	740
	# 9	G	784
	# 	A flat	831
	# print num
	freq = 0
	if num == '0':
		freq = 440
	if num == '1':
		freq = 494
	if num == '2':
		freq = 523
	if num == '3':
		freq = 554
	if num == '4':
		freq = 587
	if num == '5':
		freq = 622
	if num == '6':
		freq = 659
	if num == '7':
		freq = 698
	if num == '8':
		freq = 740
	if num == '9':
		freq = 784
	# print freq
	t = linspace(0,leng,leng*rate)
	data = sin(2*pi*freq*t)
	vibrations = sin(pi*2*freq/550*t)*2000
	# data = data + vibrations

# ===== ROLL OFF OF NOTE ======
	ampArray = numpy.ones(len(data))
	percRollOff = 0.1
	# Deffines start point for roll off
	startRollOff = int(len(ampArray)  - (percRollOff*len(ampArray)))
	# End point for roll off
	endRollOff =len(ampArray)
	# print "start and stop points"
	# print startRollOff
	# print endRollOff 
	dropInc = 1.0*amp / (endRollOff - startRollOff)
	# print "dropInc: " + str (dropInc)
	dropOffLine = numpy.arange(0,amp,(dropInc))
	revDropOffLine = dropOffLine[::-1]
	# print ampArray[startRollOff:endRollOff]
	# print revDropOffLine
	# print len(revDropOffLine)
	
	ampArray = (ampArray * amp) + vibrations
	try:
		ampArray[startRollOff:endRollOff] = revDropOffLine
	except: 
		ampArray[startRollOff-1:endRollOff] = revDropOffLine
	# print ampArray
	data=data*ampArray

	return data.astype(int16) # two byte integers

def readFile (name,StartRange,EndRange):
	with open ("Pi.txt", "r") as myfile:
		data=myfile.read().replace('\n', '')
    	# print data[StartRange:EndRange]
    	return data[StartRange:EndRange]




data = readFile("pi",0,100)
lengthOfData = len(data)
# Starts with silence
toneSum = noteNumtoName(1,0.01,amp=5000)
chordSum = noteNumtoName(1,0.01,amp=5000)
print "Entering compile loop::!"
amplitude = 5000
# DERP
for i in range(0,lengthOfData): 
	print i
	if i%10 == 0:
		print "Ding: -----------"
		if int(data[i]) == 0:
			amplitude = 5000
		if int(data[i]) != 0:
			amplitude =(1.0*(int(data[i]))/10 * 5000) +3000
		print amplitude
	if i%3 == 0:
		# print data[i]
		if int(data[i]) == 0:
			velocity = 1
		if int(data[i]) != 0:
			velocity = 1.0*(int(data[i]))/20
		
		# print type(velocity)
		print velocity

	tone = noteNumtoName(data[i],velocity,amp=amplitude)
	
	if i%4 == 0:
		tone2 = noteNumtoName(data[i-3],velocity,amp=amplitude)
		tone = tone + tone2
	if i%6 == 0:
		tone2 = noteNumtoName(data[i+2],velocity,amp=amplitude)
		tone = tone + tone2
	if i%5 == 0:
		tone2 = noteNumtoName(data[i-1],velocity,amp=amplitude)
		# tone3 = noteNumtoName(data[i+1],velocity,amp=amplitude)
		tone = tone + tone2
	toneSum = numpy.append(toneSum,tone)
	

write('Pi.wav',22000,toneSum) # writing the sound to a file

# Uses Py game to play the souind file saved by the write command
pygame.mixer.init()
soundCloser = pygame.mixer.Sound("Pi.wav")
soundCloser.play()

time.sleep(10)